FROM python:3

# If you edit the default packages installed then you need to update the .gitlab-ci.yml file
RUN apt-get update && apt-get install -y libpq-dev netcat default-mysql-client && apt-get clean
RUN cp /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -qr /tmp/requirements.txt

# Text Buffer Fix and don't write pyc files
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1



COPY . /flask-app
WORKDIR /flask-app
EXPOSE 5000
CMD ["bash", "run.sh"]
