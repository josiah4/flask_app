#!/usr/bin/env bash

# Helpers functions
function db() {
  mysql --wait -h "$MYSQL_DATABASE_HOST" --user="$MYSQL_DATABASE_USER" --password="$MYSQL_DATABASE_PASSWORD" "$MYSQL_DATABASE_DB"
}

function sql() {
  echo $@ | db
}

function quiet_sql() {
  sql $@ > /dev/null
}

# First, let's check to see if the mysql command is installed
if ! which mysql; then
  echo 'Unable to find mysql command.  Please install the MySQL client before continuing. ' \
    'If in Docker, run apt-get install default-mysql-client' >&2
  exit 1
fi

# Second, try to connect; if the mysql command is fails to process a query (through connection error, timeout, syntax
# error or unknown table/fields, it will exit with a non-zero status
if ! quiet_sql 'select 1'; then
  echo 'Error: unable to connect to database' >&2
  exit 1
fi

# Then, try to see if the tables exist
if ! quiet_sql 'select count(id) from users'; then
  echo 'Creating schema...'
  db < database/schema.sql
fi

# Then, see if some known data exists.  If the count (we don't want the header, thus `grep -v`) is equal to zero, then
# we fill the database with the initial data it needs
if [[ $(sql 'select count(id) from user_types' | grep -v 'count') -eq 0 ]]; then
  echo 'Initializing data...'
  db < database/initial_data.sql
fi

# Finally, create the first user so that the system will be useable
if [[ $(sql 'select count(id) from users' | grep -v 'count') -eq 0 ]]; then
  echo 'Creating first user...'
  python admin.py user add
fi

echo 'Database is set up'
