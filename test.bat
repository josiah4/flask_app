docker rm -f flask_db_test || true
docker run --name flask_db_test -d -e MYSQL_DATABASE=db -e MYSQL_ROOT_PASSWORD=localtest -e TZ='America/Los_Angeles' mysql:5.7
docker run -it --rm --env-file %CD%\test.env -v  %CD%:/flask-app --link flask_db_test:db --name flask-test flask-app ./test.sh
docker rm -f flask_db_test