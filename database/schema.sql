create table if not exists users
(
  id int auto_increment
    primary key,
  name varchar(50) null,
  salted_password varchar(200) null,
  email varchar(100) null,
  phone varchar(25),
  birthday date null,
  registration_date datetime null,
  address1 varchar(75) null,
  address2 varchar(75) null,
  city varchar(75) null,
  state varchar(75) null,
  postal varchar(75) null,
  active int null,
  role int null,
  last_seen datetime null,
  Unique (email)
);

create table if not exists roles
(
  id int primary key,
  role varchar(50) null,
  description longtext null
);