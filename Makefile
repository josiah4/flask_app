.PHONY: build test
build:
	docker build -t flask-app  .

pull:
	docker build --pull -t flask-app  .

debug:
	docker run -it --rm -p 5000:5000 --env-file ${PWD}/env.sh -v  ${PWD}/app:/tools/app flask-app '/bin/bash'

test:	
	docker rm -f flask_db_test || true &&\
	docker run --name flask_db_test -d -e MYSQL_DATABASE=db -e MYSQL_ROOT_PASSWORD=localtest -e TZ='America/Los_Angeles' mysql:5.7 &&\
	docker run -it --rm --env-file ${PWD}/test.env -v  ${PWD}:/flask-app --link flask_db_test:db --name flask-test flask-app './test.sh' &&\
	docker rm -f flask_db_test
