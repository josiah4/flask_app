from datetime import timedelta
import os


WTF_CSRF_ENABLED = True
SECRET_KEY = 'asdf'  # make sure to change this
PERMANENT_SESSION_LIFETIME = timedelta(hours=8)

# Amazon S3 bucket information
S3_BUCKET = os.environ.get("S3_BUCKET_NAME")
S3_KEY = os.environ.get("S3_ACCESS_KEY")
S3_SECRET = os.environ.get("S3_SECRET_ACCESS_KEY")

# Local Temp Storage
UPLOAD_FOLDER = '/flask-app/app/uploads/'
ALLOWED_EXTENSIONS = set(['csv', 'pdf', 'png'])

# Mysql config using flask-mysql (pymysql)
MYSQL_DATABASE_HOST = os.environ.get('MYSQL_DATABASE_HOST')
MYSQL_DATABASE_DB = os.environ.get('MYSQL_DATABASE_DB')
MYSQL_DATABASE_USER = os.environ.get('MYSQL_DATABASE_USER')
MYSQL_DATABASE_PASSWORD = os.environ.get('MYSQL_DATABASE_PASSWORD')

SENDER_EMAIL = os.environ.get('SENDER_EMAIL')
SMTP_PASSWORD = os.environ.get('SMTP_PASSWORD')
SMTP_SERVER = os.environ.get('SMTP_SERVER')
SMTP_PORT = int(os.environ.get('SMTP_PORT'))  # 485 or 587 accepted

DB_PASS = os.environ.get('DATABASE_PASSWORD')
database_url = 'mysql+pymysql://{user}:{password}@{host}/{database}'.format(host=MYSQL_DATABASE_HOST,
                                                                            user=MYSQL_DATABASE_USER,
                                                                            password=MYSQL_DATABASE_PASSWORD,
                                                                            database=MYSQL_DATABASE_DB)

SQLALCHEMY_DATABASE_URI = database_url
SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG = os.environ.get('DEBUG')

ADMIN_ROLES = [1, 2, 3]

TIMEZONE = os.environ.get('TIMEZONE', 'US/Pacific')
