from flask import (Blueprint, render_template, session, request, flash, url_for, redirect, jsonify, make_response, Response, send_from_directory)

from app import (config, app)
from app.views.decorators import (admin_required)

import json
from datetime import datetime, date, time

from app.helpers import send_email_gmail

api = Blueprint('api', __name__, 
        url_prefix = '/api', 
        template_folder = 'templates',
        static_folder = 'static',
        static_url_path = '/static/api'
    )


@api.route("/test", methods=['GET'])
def articles():
    list = {'test': 'worked'}
    test = json.dumps(list)
    return jsonify(test)


