from app import (db, config)

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.mutable import MutableDict, MutableList

from datetime import date, time, timedelta, datetime


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    salted_password = db.Column(db.String(200), nullable=True)
    name = db.Column(db.String(75), nullable=False)
    email = db.Column(db.String(75), nullable=True)
    phone = db.Column(db.String(25), nullable=True)
    birthday = db.Column(db.Date, nullable=True)
    registration_date = db.Column(db.Date, nullable=True)
    address1 = db.Column(db.String(75), nullable=True)
    address2 = db.Column(db.String(75), nullable=True)
    city = db.Column(db.String(75), nullable=True)
    state = db.Column(db.String(5), nullable=True)
    postal = db.Column(db.String(15), nullable=True)
    active = db.Column(db.Boolean, default=True, nullable=False)
    role = db.Column(db.Integer, default=5, nullable=False)
    last_seen = db.Column(db.Date, nullable=True)


class Role(db.Model):
    """Lookup table for roles"""

    __tablename__ = "roles"

    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(750), nullable=True)