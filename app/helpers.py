from flask import (session)
from passlib.hash import argon2

import smtplib
import ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from app import (config, app, mysql, db)


def send_email_gmail(subject, receiver_email, html, attachment=False, attachment_name=False, sender_email=False):
    """

    Args:
        subject (str): Subject of the email
        receiver_email (str): Recipients of the email
        html (str): html string for email
        attachment:
        attachment_name (str): Name of the attachement
        sender_email:

    Returns:
        boolean
    """
    if not sender_email:
        sender_email = config.SENDER_EMAIL
    context = ssl.create_default_context()

    # Create multipart message and set headers
    message = MIMEMultipart()
    message['From'] = config.SENDER_EMAIL
    message['To'] = receiver_email
    message['Subject'] = subject

    # Add body to email
    message.attach(MIMEText(html, 'html'))

    # Add Attachment
    if attachment:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment)
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {attachment_name}",
        )
        message.attach(part)

    # Send message with TLS/STARTTLS
    if config.SMTP_PORT == 587:
        try:
            with smtplib.SMTP(config.SMTP_SERVER, config.SMTP_PORT) as server:
                server.starttls(context=context)
                server.login(config.SENDER_EMAIL, config.SMTP_PASSWORD)
                server.sendmail(sender_email, receiver_email, message.as_string())
            return True
        except Exception as e:
            print(f'There was an error in send_email_gmail()  {repr(e)}')

    # Send message with SSL
    elif config.SMTP_PORT == 485:
        try:
            with smtplib.SMTP_SSL(config.SMTP_SERVER, config.SMTP_PORT, context=context) as server:
                server.login(config.SENDER_EMAIL, config.SMTP_PASSWORD)
                server.sendmail(sender_email, receiver_email, message)
            return True
        except Exception as e:
            print(f'There was an error in send_email_gmail()  {repr(e)}')

    # Return False on error
    return False


def salt_password(password):
    hash = argon2.using(rounds=4).hash(password)
    return hash


class PasswordStrength:

    def __init__(self):
        self.max_length = 20
        self.min_length = 8
        self.min_lower_case = 1
        self.min_numeric = 1
        self.min_symbol = 0
        self.min_upper_case = 1
        self.min_diacritic = 0

        self.NUMERIC_DIGIT_MIN = u'\u0030'
        self.NUMERIC_DIGIT_MAX = u'\u0039'

        self.UPPER_CASE_LETTER_MIN = u'\u0041'
        self.UPPER_CASE_LETTER_MAX = u'\u005A'

        self.LOWER_CASE_LETTER_MIN = u'\u0061'
        self.LOWER_CASE_LETTER_MAX = u'\u007A'

        self.SYMBOL_CHARS = (
                u'\u0020',  #
                u'\u0021',  # !
                u'\u0022',  # "
                u'\u0023',  # #
                u'\u0024',  # $
                u'\u0025',  # %
                u'\u0026',  # &
                u'\u0027',  # '
                u'\u0028',  # (
                u'\u0029',  # )
                u'\u002A',  # *
                u'\u002B',  # +
                u'\u002C',  # ,
                u'\u002D',  # -
                u'\u002E',  # .
                u'\u002F',  # /
                u'\u003A',  # :
                u'\u003B',  # ;
                u'\u003C',  # <
                u'\u003D',  # =
                u'\u003E',  # >
                u'\u003F',  # ?
                u'\u0040',  # @
                u'\u005B',  # [
                u'\u005C',  # \
                u'\u005D',  # ]
                u'\u005E',  # ^
                u'\u005F',  # _
                u'\u0060',  # `
                u'\u007B',  # {
                u'\u007D',  # }
                u'\u007E',  # ~
                u'\u007C',  # |
                u'\u00A1',  # ¡
                u'\u00A6',  # ¦
                u'\u00A7',  # §
                u'\u00A9',  # ©
                u'\u00AB',  # «
                u'\u00AC',  # ¬
                u'\u00AE',  # ®
                u'\u00B1',  # ±
                u'\u00B5',  # µ
                u'\u00B6',  # ¶
                u'\u00B7',  # ·
                u'\u00BB',  # »
                u'\u00BD',  # ½
                u'\u00BF',  # ¿
                u'\u00D7',  # ×
                u'\u00F7')  # ÷

        self.DIACRITIC_CHARS = (
                u'\u00C0',  # À
                u'\u00C1',  # Á
                u'\u00C2',  # Â
                u'\u00C3',  # Ã
                u'\u00C4',  # Ä
                u'\u00C5',  # Å
                u'\u00C6',  # Æ
                u'\u00C7',  # Ç
                u'\u00C8',  # È
                u'\u00C9',  # É
                u'\u00CA',  # Ê
                u'\u00CB',  # Ë
                u'\u00CC',  # Ì
                u'\u00CD',  # Í
                u'\u00CE',  # Î
                u'\u00CF',  # Ï
                u'\u00D0',  # Ð
                u'\u00D1',  # Ñ
                u'\u00D2',  # Ò
                u'\u00D3',  # Ó
                u'\u00D4',  # Ô
                u'\u00D5',  # Õ
                u'\u00D6',  # Ö
                u'\u00D8',  # Ø
                u'\u00D9',  # Ù
                u'\u00DA',  # Ú
                u'\u00DB',  # Û
                u'\u00DC',  # Ü
                u'\u00DD',  # Ý
                u'\u00DE',  # Þ
                u'\u00DF',  # ß
                u'\u00E0',  # à
                u'\u00E1',  # á
                u'\u00E2',  # â
                u'\u00E3',  # ã
                u'\u00E4',  # ä
                u'\u00E5',  # å
                u'\u00E6',  # æ
                u'\u00E7',  # ç
                u'\u00E8',  # è
                u'\u00E9',  # é
                u'\u00EA',  # ê
                u'\u00EB',  # ë
                u'\u00EC',  # ì
                u'\u00ED',  # í
                u'\u00EE',  # î
                u'\u00EF',  # ï
                u'\u00F0',  # ð
                u'\u00F1',  # ñ
                u'\u00F2',  # ò
                u'\u00F3',  # ó
                u'\u00F4',  # ô
                u'\u00F5',  # õ
                u'\u00F6',  # ö
                u'\u00F8',  # ø
                u'\u00F9',  # ù
                u'\u00FA',  # ú
                u'\u00FB',  # û
                u'\u00FC',  # ü
                u'\u00FD',  # ý
                u'\u00FE',  # þ
                u'\u00FF')  # ÿ

    def validate_password(self, password):
        if len(password) > self.max_length:
            return 'Password exceeded the maximum length of {}'.format(self.max_length)

        if len(password) < self.min_length:
            return 'Password minimum length of {} not satisfied.'.format(self.min_length)

        num_of_lower_case = sum(1 for c in password if (
            self.LOWER_CASE_LETTER_MIN <= c <= self.LOWER_CASE_LETTER_MAX))

        if num_of_lower_case < self.min_lower_case:
            return 'Password requires at least {} lowercase characters.'.format(self.min_lower_case)

        num_of_numeric = sum(
            1 for c in password if (
                self.NUMERIC_DIGIT_MIN <= c <= self.NUMERIC_DIGIT_MAX))

        if num_of_numeric < self.min_numeric:
            return 'Password requires at least {} numeric characters.'. format(self.min_numeric)

        num_of_symbol = sum(
            1 for c in password if c in self.SYMBOL_CHARS)

        if num_of_symbol < self.min_symbol:
            return 'Password requires at least {} symbol characters.'.format(self.min_symbol)

        num_of_upper_case = sum(1 for c in password if (
            self.UPPER_CASE_LETTER_MIN <= c <= self.UPPER_CASE_LETTER_MAX))

        if num_of_upper_case < self.min_upper_case:
            return 'Password requires at least {} uppercase characters.'.format(self.min_upper_case)

        num_of_diacritic = sum(
            1 for c in password if c in self.DIACRITIC_CHARS)

        if num_of_diacritic < self.min_diacritic:
            return 'Password requires at least {} diacritic characters.'.format(self.min_diacritic)
        return False