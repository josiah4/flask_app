from passlib.hash import argon2
from datetime import datetime


from app import app
from app.views.decorators import *
from app.models import User
from app.forms import LoginForm, RegisterForm
from app.helpers import PasswordStrength

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from app import (db)

from pprint import pprint as pp


#######################################
### record and get user information ###
#######################################

@app.errorhandler(404)
def not_found(e):
    # print(repr(e))
    return render_template("404.html")


@app.errorhandler(500)
def not_found(e):
    print(repr(e))
    return render_template("500.html")

@app.route("/register", methods=['GET', 'POST'])
def register():
    if 'username' in session:
        flash('You are already signed in and can not register unless you sign out.')
        return redirect(url_for('home'))
    form = RegisterForm()
    if form.validate_on_submit():
        if form.password.data == form.confirm_password.data:
            not_strong = PasswordStrength().validate_password(form.password.data)
            if not_strong:
                flash('You must enter a secure password with 8 characters and three of the '
                      'following: upper case, lower case, number, symbol', 'error')
                return render_template('register.html', form=form)
        else:
            flash('Your passwords must match', 'error')
            return render_template('register.html', form=form)
        new_user = User(
            salted_password=argon2.using(rounds=4).hash(form.password.data),
            name=form.name.data,
            email=form.email.data,
            registration_date=datetime.now()
        )
        db.session.add(new_user)
        try:
            db.session.commit()
        except IntegrityError:
            flash('That email is already in the system. Please try another email or use the forgot password link',
                  'error')
            return render_template('register.html', form=form)
        session['username'] = form.email.data
        if 'next' in session:
            next = session['next']
            session.pop('next', None)
            return redirect(next)
        return redirect(url_for('home'))
    return render_template('register.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'username' in session:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter(User.email == form.user.data).one()
        except NoResultFound:
            flash('The username or password you entered do not match please try again', 'error')
            return redirect(url_for('login'))
        if argon2.verify(form.password.data, user.salted_password):
            session['username'] = user.email
            if 'next' in session:
                next = session['next']
                session.pop('next', None)
                return redirect(next)
            return redirect(url_for('home'))
        else:
            flash('The username or password you entered do not match please try again', 'error')
            return redirect(url_for('login'))
    return render_template("login.html",
                           form=form,
                           nomenu=True)


@app.route("/logout")
@login_required
def logout():
    if 'username' in session:
        session.pop('username', None)
        session.pop('firstName', None)
        session.pop('lastName', None)
        session.pop('email', None)
        session.pop('companyRole', None)
        session.pop('company', None)
        session.pop('visuallyImpaired', None)
        session.pop('userType', None)
        session.pop('billing', None)
    return redirect(url_for('home'))


##################################################
###             Home Dashboards                ###
##################################################

@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('home.html')