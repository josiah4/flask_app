from flask import render_template, session, request, flash, url_for, redirect, jsonify, make_response, Response
from functools import wraps

##################################
#### Confirm user credentials ####
##################################


def login_required(func):
    """Decorator to redirect to login."""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        """shell function."""
        if 'username' not in session:
            session['next'] = request.url
            return redirect(url_for('login'))
        return func(*args, **kwargs)
    return decorated_view


def admin_required(func):
    """Decorator to redirect to login."""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        """shell function."""
        if session['companyRole'] != 1:
            if 'username' in session:
                flash('You do not have permissions to access this part of the site.')
                return redirect(url_for('home'))
            else:
                session['next'] = request.url
                return redirect(url_for('login'))
        return func(*args, **kwargs)
    return decorated_view
