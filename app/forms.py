from flask_wtf import (FlaskForm)
from flask_wtf.file import (FileField, FileAllowed, FileRequired)
from wtforms import (StringField, PasswordField, SelectField, IntegerField, 
                     DateField, BooleanField, RadioField, TextAreaField, 
                     SubmitField, FloatField, SelectMultipleField, widgets,
                     Form, FormField, FieldList)
from wtforms.validators import (DataRequired, EqualTo, Length, Email, 
                                Optional, ValidationError)
from wtforms.fields.html5 import (EmailField, TelField, DateTimeField, 
                                  DateTimeLocalField, TimeField)
from wtforms_alchemy.fields import (QuerySelectField)

import datetime


class LoginForm(FlaskForm):
    user = EmailField('Eamil', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    submit = SubmitField('sign in')


class RegisterForm(FlaskForm):
    name = StringField('First Name', validators=[DataRequired()])
    email = EmailField('Email Address', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('register')
