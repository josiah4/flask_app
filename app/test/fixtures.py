import pytest

from app import (config, app, db)
from app import mysql

from pprint import pprint as pp
import json
from datetime import datetime, date, time, timedelta
from bs4 import BeautifulSoup

from Modules import sqlfunctions as sql


from app.models import (EmployeeCalendar, Employee, Customer, ServiceType)




@pytest.fixture
def service_types():
    
    service_types = list()

    types = [
              {
                "id": 1, 
                "code": "OR526",
                "description": "Attendant Care, ADL/IADL",
                "name": "Hours",
                "unit_measure": "Hours",
                "modifier": "NA"
              },
              {
                "id": 2,
                "code": "OR004",
                "description": "Community transportation/mileage",
                "name": "Transportation",
                "unit_measure": "Miles",
                "modifier": "WD"
              },
              {
                "id": 3,
                "code": "OR401",
                "description": "Individual Supported Employment",
                "name": "Job Coaching",
                "unit_measure": "Hours",
                "modifier": "NA"
              },
              {
                "id": 4,
                "code": "OR507",
                "description": "Relief Care, Daily",
                "name": "Relief Care",
                "unit_measure": "Days",
                "modifier": "NA"
              },
              {
                "id": 5,
                "code": "OR542",
                "description": "Community/Group Care",
                "name": "Group Care",
                "unit_measure": "Hours",
                "modifier": "W2"
              },
              {
                "id": 6,
                "code": "OR542",
                "description": "1 on 1 Staff Support",
                "name": "Staff Support",
                "unit_measure": "Hours",
                "modifier": "R1"
              },
              {
                "id": 7,
                "code": "OR526",
                "description": "2 on 1 care",
                "name": "2 on 1 Care",
                "unit_measure": "Hours",
                "modifier": "ZE"
              }
            ]

    for t in types:
        s = ServiceType(**t)
        db.session.add(s)
        db.session.commit()

        service_types.append({'id_': s.id, 'obj': s})

    return service_types


def get_emplid(user_id):
    emplid = None

    conn = mysql.connect()

    with conn.cursor() as cursor:
        cursor.execute("select * from employees where userId = %s", (user_id,))
        for row in cursor:
            emplid = row['id']
    conn.close()

    return emplid

@pytest.fixture
def agency_good_place():
    agency_id = None

    agents = sql.agencies()

    agents.add_agency("The Good Place", "good@example.org", "111-111-1111", "1111", "Someplace", "Very Good", "Newberg",
                      "OR", "97306")
    all_agencies = agents.get_agencyList()

    for a in all_agencies:

        if a['name'] == "The Good Place":
            agency_id = a['id']

    return agency_id


@pytest.fixture
def service_coordinator_bella(agency_good_place):
    coordinator_id = None

    agents = sql.agencies()

    agents.add_serviceCoordinator("Bella Enchanted", 'bella@example.org', "222-222-2222", None, agency_good_place,
                                  False)

    cordinators = agents.get_serviceCoordinatorList(agency=agency_good_place)

    for c in cordinators:
        if c['name'] == "Bella Enchanted":
            coordinator_id = c["id"]

    return coordinator_id


# Need a customer
@pytest.fixture
def customer_john_doe(agency_good_place, service_coordinator_bella):
    customer = None
    customer_id = None
    #
    first = "John"
    last = "Doe"
    primeNum = "0000000000"

    provider = 1  # set in inital data
    agency = agency_good_place
    serviceCoordinator = service_coordinator_bella

    authServices = [(2, '10', '10', 2), (1, '10', '10', 2)]
    approvedTasks = [(' ', 'on')]
    identifiedRisks = ['']

    contractStart = (datetime.now() - timedelta(5)).strftime("%Y-%m-%d")
    contractExp = (datetime.now() + timedelta(5)).strftime("%Y-%m-%d")

    newCustomer = True
    customerID = None
    customFields = [{'id': '0', 'description': '', 'information': ''}]

    customer = sql.customers()
    result = customer.add_client(first, last, primeNum, provider, agency, serviceCoordinator, authServices,
                                 approvedTasks, identifiedRisks, contractStart, contractExp, newCustomer, customerID,
                                 customFields)

    customer_id = customer.get_customerid(primeNum)

    return customer_id


@pytest.fixture
def customer_jane_doe(agency_good_place, service_coordinator_bella):
    customer = None
    customer_id = None
    #
    first = "Jane"
    last = "Doe"
    primeNum = "0000000001"

    provider = 1  # set in inital data
    agency = agency_good_place
    serviceCoordinator = service_coordinator_bella

    authServices = [(2, '10', '10', 2), (1, '10', '100', 2)]
    approvedTasks = [(' ', 'on')]
    identifiedRisks = ['']

    contractStart = (datetime.now() - timedelta(5)).strftime("%Y-%m-%d")
    contractExp = (datetime.now() + timedelta(5)).strftime("%Y-%m-%d")

    newCustomer = True
    customerID = None
    customFields = [{'id': '0', 'description': '', 'information': ''}]

    customer = sql.customers()
    result = customer.add_client(first, last, primeNum, provider, agency, serviceCoordinator, authServices,
                                 approvedTasks, identifiedRisks, contractStart, contractExp, newCustomer, customerID,
                                 customFields)

    customer_id = customer.get_customerid(primeNum)

    return customer_id

@pytest.fixture
def customer_jane_doe_user(agency_good_place, service_coordinator_bella, customer_jane_doe, employee_bugs_bunny):
    profile = sql.Profile()
    customer = sql.customers()

    # Define the user parameters
    email = 'test_customer@example.org'
    username = 'jane'
    password = 'test'
    first = "Jane"
    last = "Doe"
    primeNum = "0000000001"
    agency = agency_good_place
    serviceCoordinator = service_coordinator_bella
    customerID = customer_jane_doe
    employee_relationship = [employee_bugs_bunny]

    # Profile myst have an email address before the user is created.
    customer_message = customer.edit_customerInfo(customerID, first, last, serviceCoordinator, primeNum, email, None,
                                                  None, None, None, None, None, None, None, None, None, None, None, None,
                                                  agency, None, None, None, None, None, employee_relationship, 1, None)

    # Create the user
    user_message = profile.create_user(email, username, password, 2)  # Customer user type

    user_id = profile.get_userID(username)

    return user_id


@pytest.fixture
def employee_bugs_bunny():
    employee_id = None

    employee = sql.employees()
    profile = sql.Profile()

    first_name = "Bugs"
    last_name = "Bunny"
    email = 'bugs@example.org'
    username = "bugs"
    password = "test"

    message = profile.create_user(email, username, password, 1)  # Employee user type
    user_id = profile.get_userID(username)

    hire_date = datetime.now().date()
    err = employee.add_employee(first_name, last_name, email,
                                None, None, None, None, None, None, None,  # Demographics
                                None, None, None, None, '97132',  # Mailing Address
                                True,  # active
                                4,  # Employee
                                user_id,
                                hire_date,
                                None)

    employee_id = get_emplid(user_id)

    return employee_id


@pytest.fixture
def employee_daffy_duck():
    employee_id = None

    employee = sql.employees()
    profile = sql.Profile()

    first_name = "Daffy"
    last_name = "Duck"
    email = 'daffy@example.org'
    username = "daffy"
    password = "test"

    message = profile.create_user(email, username, password, 1)  # Employee user type
    user_id = profile.get_userID(username)

    hire_date = datetime.now().date()
    err = employee.add_employee(first_name, last_name, email,
                                '(971) 999-0000', None, None, None, None, None, '97132',  # Demographics
                                None, None, None, None, '97132',  # Mailing Address
                                True,  # active
                                4,  # Employee
                                user_id,
                                hire_date, None)

    employee_id = get_emplid(user_id)

    return employee_id


@pytest.fixture
def employee_elmer_fudd():
    employee_id = None

    employee = sql.employees()
    profile = sql.Profile()

    first_name = "Elmer"
    last_name = "Fudd"
    email = 'elmer@example.org'
    username = "elmer"
    password = "test"

    message = profile.create_user(email, username, password, 1)  # Employee user type
    user_id = profile.get_userID(username)

    hire_date = datetime.now().date()
    err = employee.add_employee(first_name, last_name, email,
                                None, None, None, None, None, None, None,  # Demographics
                                None, None, None, None, '97132',  # Mailing Address
                                True,  # active
                                4,  # Employee
                                user_id,
                                hire_date, None)

    employee_id = get_emplid(user_id)

    return employee_id


@pytest.fixture
def customer_relationship_john_doe_and_daffy_duck(customer_john_doe, employee_daffy_duck):
    relationship_id = None
    company_id = 1

    sql = "INSERT INTO custEmplRel (customerID, companyID, employeeID) VALUES (%s, %s, %s)"
    params = (customer_john_doe, company_id, employee_daffy_duck)

    conn = mysql.connect()

    with conn.cursor() as cursor:
        cursor.execute(sql, params)
        relationship_id = cursor.lastrowid

    conn.commit()
    conn.close()

    return relationship_id

@pytest.fixture
def build_exprs_file():
    authServices = [(2, '10', '10', 2), (1, '10', '10', 2)]
    approvedTasks = [(' ', 'on')]
    identifiedRisks = ['']

    contract_start = (datetime.now() - timedelta(5)).strftime("%m/%d/%Y")
    contract_exp = (datetime.now() + timedelta(5)).strftime("%m/%d/%Y")

    rows = [ [24881289, '0000000000', 'John Doe', 151, 'OR526', 'NA', 10, 148058, contract_start, contract_exp,
           1, 1, 1053620, 1, '$0.485', '$485.00',
           'No', 'Accepted'], [24881289, '0000000000', 'John Doe', 151, 'OR0004', 'WD', 10, 148058, contract_start, contract_exp,
           1, 1, 1053620, 1, '$0.485', '$485.00',
           'No', 'Accepted']]
    customer_sql = sql.customers()
    for row in rows:
        uploaded = customer_sql.upload_spa(row)
