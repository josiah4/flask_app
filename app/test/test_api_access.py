import pytest
import json

from app import (config, app)

from pprint import pprint as pp

class TestApiAccess():

    def setup(self):

        app.config['TESTING'] = True # Let the app know we are testing
        self.app = app.test_client()

    def test_articles_fail_if_not_logged_in(self):
        
        resp = self.app.get('/api/test', follow_redirects=True)
        
        # Make sure we were redirected to the login page
        assert 'test' in resp.data.decode('utf-8')
