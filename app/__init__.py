from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

app = Flask(__name__)
app.config.from_object('app.config')
mysql = MySQL(cursorclass=DictCursor)
mysql.init_app(app)

db = SQLAlchemy(app)

Bootstrap(app)

from app import views
from app.api.views import api

app.register_blueprint(api)

if __name__ == "__main__":
    app.run()
