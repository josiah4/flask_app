#!/usr/bin/env python
import click
from datetime import datetime, timedelta
from app.models import User
from app.helpers import salt_password
from app import db


@click.group()
def cli():
    """
    Commands for managing the CRM
    """


@cli.group()
def user():
    """
    User management commands
    """


@user.command()
@click.option('--email', '-e', prompt=True)
@click.option('--username', '-u', prompt=True)
@click.password_option('--password', '-p')
@click.option('--first-name', '-fn', prompt=True)
@click.option('--last-name', '-ln', prompt=True)
def add(email, username, password, first_name, last_name):
    """
    Adds a new CRM administrator, AKA a user as an employee with Owner role.
    """
    today = datetime.now()
    birthday = today - timedelta(days=10000)
    hash_password = salt_password(password)
    new_user = User(salted_password=hash_password,
                    name=f"{first_name} {last_name}",
                    email=email,
                    phone='123-456-7890',
                    birthday=birthday.date(),
                    registration_date=today,
                    address1='123 test ln',
                    city='city',
                    state='AB',
                    postal='95823',
                    active=True,
                    role=1)

    db.session.add(new_user)
    db.session.commit()


if __name__ == "__main__":
    cli()
