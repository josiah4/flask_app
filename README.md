# Base Flask App

## Build

### Set-up env.sh
copy the `env.sh.dist` to `env.sh` and enter details

```bash
cp env.sh.dist env.sh
``` 

### Staging

Build and run docker container

```bash
# If on Windows this must be run in Powershell
docker build -t app .
docker run --rm -it -p 5000:5000 --name app --env-file ${PWD}/env.sh -v ${PWD}/app:/website/app -v ${PWD}/Modules:/website/Modules app '/bin/bash'
# Once in the docker container run:
./run.sh
```

Connect to container at [localhost:5000](http://localhost:5000)

## Local Development with Docker Compose

1. From the Project directory run: `docker-compose up`
2. If you have development data to work with load that with `mysql -h 127.0.0.1 -u root -D db -plocaldev < my-dev-data.sql`
3. To shutdown and cleanup run: `docker-compose down`


## Running Tests

### On Linux
```bash
make build # Required if it is your first time running tests or if you changed dependencies

make test
```
### On Windows
```bash
.\test.bat
```

## Other Notes

### To see the ddl sqlalchemy would generate you can run

```bash
python -c "from app import db; from app.models import EmployeeCalendar; db.create_all()"
```

Then look at the table's create statement
  
## Automated tests in gitlab

These tests run on every push, but you must tag a commit in order to have it update the latest docker container. 

To tag a commit run the command
```shell
git tag -a <version number> -m "<comments>"
git push origin <version number>
```
These test are manage in the gitlab-ci.yml file

#Themed Bootstrap
If you are using the themee bootstrap form this project you must purchase the overlay here: https://themes.getbootstrap.com/product/sparrow-simple-seamless-alive/
 