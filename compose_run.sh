#!/bin/bash

function db() {
  mysql --wait -h "$MYSQL_DATABASE_HOST" --user="$MYSQL_DATABASE_USER" --password="$MYSQL_DATABASE_PASSWORD" "$MYSQL_DATABASE_DB"
}


function sql() {
  echo $@ | db
}

function quiet_sql() {
  sql $@ > /dev/null
}

until mysql -h ${MYSQL_DATABASE_HOST} -u ${MYSQL_DATABASE_USER} -p${MYSQL_DATABASE_PASSWORD} ${MYSQL_DATABASE_DB} -e 'select 1'; do
    >&2 echo "MySQL is unavailable - sleeping"
    sleep 1
done

>&2 echo "Mysql is up - init db"
db < database/schema.sql
db < database/initial_data.sql

>&2 echo "Add Test User"
python admin.py user add -e dev@example.org -p dev --first-name Local --last-name Developer

python debug.py
